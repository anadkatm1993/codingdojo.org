package leapyear_new.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import leapyear_new.service.LeapYearService;

@RestController
public class LeapYearController {

	@Autowired
	private LeapYearService leapYearService;
	
	@GetMapping("/checkleapyear/{year}")
	public ResponseEntity<Boolean> checkLeapYear(@PathVariable("year")long year)
	{
		boolean response = leapYearService.checkLeapYear(Long.valueOf(year));
		return ResponseEntity.ok(response);
	}
}
