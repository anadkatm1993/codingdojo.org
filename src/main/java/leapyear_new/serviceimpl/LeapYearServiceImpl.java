package leapyear_new.serviceimpl;

import org.springframework.stereotype.Service;

import leapyear_new.service.LeapYearService;

/**
 * This class is used to implement business logic
 * 
 * @author meera
 *
 */

@Service
public class LeapYearServiceImpl implements LeapYearService {

	/**
	 * This method is used to check if the year is a leap year
	 */
	@Override
	public boolean checkLeapYear(Long year) {
		boolean isLeapYear = false;
		// check if year is divisible by 4
		if (year % 4 == 0) {
			// check if year is divisible by 100
			if (year % 100 == 0) {
			 // check if year is divisible by 400
				if (year % 400 == 0)
					isLeapYear = true;
				else
					isLeapYear = false;
			} else
				isLeapYear = true;
		} else
			isLeapYear = false;
		return isLeapYear;
	}

}
