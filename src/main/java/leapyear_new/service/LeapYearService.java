package leapyear_new.service;

public interface LeapYearService {

	public boolean checkLeapYear(Long year);
}
