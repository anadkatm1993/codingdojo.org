package leapyear_new;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import leapyear_new.serviceimpl.LeapYearServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class LeapYearServiceImplTest {

	@InjectMocks
	LeapYearServiceImpl leapYearServiceImpl;
	
@Test
public void isDivisibleBy400PositiveTest(){
    assertEquals(leapYearServiceImpl.checkLeapYear(2000L),true);
}

@Test
public void isDivisibleBy400NegativeTest(){
    assertEquals(leapYearServiceImpl.checkLeapYear(1800L),false);
}

@Test
public void isDivisibleBy100ButNotBy400NegativeTest(){
    assertEquals(leapYearServiceImpl.checkLeapYear(1900L),false);
}

@Test
public void isDivisibleBy4ButNotBy100PositiveTest(){
    assertEquals(leapYearServiceImpl.checkLeapYear(2020L),true);
}	

@Test
public void isNotDivisibleBy4NegativeTest(){
    assertEquals(leapYearServiceImpl.checkLeapYear(2021L),false);
}

}
